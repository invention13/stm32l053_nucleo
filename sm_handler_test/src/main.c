/*
*****************************************************************************

  File        : main.cpp

  The MIT License (MIT)
  Copyright (c) 2019 STMicroelectronics

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*****************************************************************************
*/

#include<stdio.h>

typedef enum { ENTRANCE, EXIT, EVENT_A, EVENT_B, EVENT_C } Event_t ;

typedef struct Context_tag
{
	void * current_state;
	void * next_state;
	// Any auxiliary information can go here

}Context_t;

typedef void (*State_t)( Context_t *, Event_t e );
void state_A( Context_t *c,  Event_t e);
void state_B( Context_t *c,  Event_t e);
void update_state( Context_t *c, Event_t e );

Context_t context = { .current_state = state_A, .next_state = state_A };

// State handler functions
void state_A( Context_t *c,  Event_t e)
{

	switch( e)
	{
	case ENTRANCE:
		// do entrance actions
		break;
	case EXIT:
		// do exit actions
		break;
	case EVENT_A:
		break;
	case EVENT_B:
		c->next_state = state_B;
		break;
	default:
		break;
	}
}

void state_B( Context_t *c,  Event_t e)
{

	switch( e)
	{
	case ENTRANCE:
		break;
	case EXIT:
		break;
	case EVENT_A:
		c->next_state = state_A;
		break;
	case EVENT_B:
		break;
	default:
		break;
	}
}

// This function can handle events in multiple threads so must be re-entrant
void update_state( Context_t *c, Event_t e )
{
	// Default for handlers is to stay in the same state
	State_t current = (State_t)c->current_state;
	c->next_state = c->current_state;

	// Evaluate transition, get next state
	current( c, e );

	// Can handle events here which must be handled regardless of state - eg RESET events

	// See if next state is the same as current one, if not, do exit and entrance actions
	if( c->current_state != c->next_state )
	{
		State_t next = c->next_state;
		current( c, EXIT) ;
		next( c, ENTRANCE );
		c->current_state = c->next_state;
	}
}

void initial_transition( Context_t *c )
{
	State_t current = c->current_state;

	current( c, ENTRANCE );
}

/*

void task(void *args)
{
	Context_t *p_context = get_context( args );

	// initialization stuff here

	initial_transition( p_context );

	while( get_next_event(&e) )
		update_state( p_context, e );
}

*/

int main(void)
{
	Event_t e[] = { EVENT_A, EVENT_B, EVENT_A, EVENT_C };
	int n_events = sizeof(e)/sizeof(Event_t);
	int i;

	initial_transition( &context );

	for( i = 0; i < n_events; i++ )
	{
		update_state( &context, e[i] );
	}




	return 0;
}
