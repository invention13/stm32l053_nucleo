/*
 * port.h
 *
 *  Created on: Feb 19, 2020
 *      Author: ian
 */

#ifndef PORT_H_
#define PORT_H_

typedef struct HAL_tag
{
	// Pointers to functions implementing API
	int x;
}HAL_t;

typedef struct DPM_tag
{
	int y;
}DPM_t;

typedef struct Protocol_tag
{
	// Message queue
	int z;
}Protocol_t;

typedef struct Policy_tag
{
	// Message queue
	int xx;
}Policy_Engine_t;

typedef struct Port_tag
{
	HAL_t hal;
	DPM_t dpm;
	Protocol_t protocol;
	Policy_Engine_t policy_engine;

}Port_t;

#endif /* PORT_H_ */
