/*
 * state.h
 *
 *  Created on: Feb 18, 2020
 *      Author: ian
 */

#ifndef STATE_H_
#define STATE_H_
#include "event.h"

typedef void *(*State_Handler_t)(void *, Event_t *);

#endif /* STATE_H_ */
