/*
 * event.h
 *
 *  Created on: Feb 18, 2020
 *      Author: ian
 */

#ifndef EVENT_H_
#define EVENT_H_

#include <stdint.h>

// Event Signals
typedef enum
{
	ENTRANCE_EVENT,
	EXIT_EVENT
	// Received Message Events
	// Status Events,
	// Timer Events
}Event_Sig_t;


// Events with signal and parameters
typedef struct
{
	Event_Sig_t sig;
	uint8_t u8_Len;
	uint8_t *pu8_Data;

}Rx_Msg_t;

typedef struct No_Data_tag
{
	Event_Sig_t sig;
}No_Data_t;

typedef union
{
	No_Data_t No_Data;
	Rx_Msg_t Rx_Msg;

}Event_Param_t;


// Union event type to send to message queues
typedef struct Event_tag
{
	Event_Sig_t sig;
	Event_Param_t params;
}Event_t;


#endif /* EVENT_H_ */
