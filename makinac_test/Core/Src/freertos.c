/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
osThreadId taskAHandle;
osThreadId taskBHandle;
static QueueHandle_t test_queue;

/* USER CODE END Variables */
osThreadId defaultTaskHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
void StartTaskA(void const * argument);
void StartTaskB(void const * argument);

void v_Set_Test_Out( uint8_t b_Enable );
void v_sendto_A_from_ISR(void);
void v_sendto_A(void);
void v_Set_User_LED(uint8_t b_On);

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void vApplicationIdleHook(void);
void vApplicationTickHook(void);

/* USER CODE BEGIN 2 */
__weak void vApplicationIdleHook( void )
{
   /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
   to 1 in FreeRTOSConfig.h. It will be called on each iteration of the idle
   task. It is essential that code added to this hook function never attempts
   to block in any way (for example, call xQueueReceive() with a block time
   specified, or call vTaskDelay()). If the application makes use of the
   vTaskDelete() API function (as this demo application does) then it is also
   important that vApplicationIdleHook() is permitted to return to its calling
   function, because it is the responsibility of the idle task to clean up
   memory allocated by the kernel to any task that has since been deleted. */
}
/* USER CODE END 2 */

/* USER CODE BEGIN 3 */

static uint16_t u16_debounce_cnt;
void v_Sw_Debounce_Out( uint8_t b_Enable );

__weak void vApplicationTickHook( void )
{
   /* This function will be called by each tick interrupt if
   configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h. User code can be
   added here, but the tick hook is called from an interrupt context, so
   code must not attempt to block, and only the interrupt safe FreeRTOS API
   functions can be used (those that end in FromISR()). */

	// Debounces blue button - produces a single 1 ms pulse on output pin.
	if( !HAL_GPIO_ReadPin( B1_GPIO_Port, B1_Pin) )
	{
		if( ++u16_debounce_cnt == 30 )
		{
		    v_Sw_Debounce_Out( 1 );
		    v_sendto_A_from_ISR();
		}
		else
		{
			v_Sw_Debounce_Out( 0 );
		}
	}
	else
	{
		u16_debounce_cnt = 0;
		v_Sw_Debounce_Out( 0 );

	}


}
/* USER CODE END 3 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
	test_queue = xQueueCreate( 5, sizeof( uint16_t ));

  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */

  osThreadDef(taskA, StartTaskA, osPriorityNormal, 0, 128);
  taskAHandle = osThreadCreate(osThread(taskA), NULL);

  osThreadDef(taskB, StartTaskB, osPriorityNormal, 0, 128);
  taskBHandle = osThreadCreate(osThread(taskB), NULL);

  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1000);
    v_Set_User_LED(1);
    osDelay(50);
    v_Set_User_LED(0);
  }
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

void StartTaskA(void const * argument)
{
  uint16_t test_buf;

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
	  xQueueReceive( test_queue, &test_buf, portMAX_DELAY );
	  v_Sw_Debounce_Out(1);
	  osDelay(100);
	  v_Sw_Debounce_Out(0);
  }
  /* USER CODE END StartDefaultTask */
}

void StartTaskB(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
	  osDelay(500);
	  v_Set_Test_Out( 1 );
	  v_sendto_A();
	  osDelay(10);
	  v_Set_Test_Out( 0 );
  }
  /* USER CODE END StartDefaultTask */
}

void v_sendto_A_from_ISR(void)
{
	uint16_t u16_msg;

	 xQueueSendFromISR( test_queue, &u16_msg, NULL );
}

void v_sendto_A(void)
{
	uint16_t u16_msg;

	 xQueueSend( test_queue, &u16_msg, 0 );
}
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
